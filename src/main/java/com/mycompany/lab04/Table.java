/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04;

/**
 *
 * @author User
 */
public class Table {

    private String[] table = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private Player player1, player2, currentPlayer;
    private int count = 1;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public String[] getTable() {
        return table;
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    

    public boolean setPosition(int position) {
        if (table[position - 1] == "1" || table[position - 1] == "2" || table[position - 1] == "3" || table[position - 1] == "4" || table[position - 1] == "5" || table[position - 1] == "6" || table[position - 1] == "7" || table[position - 1] == "8" || table[position - 1] == "9") {
            table[position - 1] = currentPlayer.getSymbol();
            this.count++;
            return true;
        }
        return false;
    }

    public boolean checkPosition() {
        if (table[0] == "X" && table[1] == "X" && table[2] == "X") {
            return true;
        } else if (table[0] == "O" && table[1] == "O" && table[2] == "O") {
            return true;
        } else if (table[0] == "X" && table[3] == "X" && table[6] == "X") {
            return true;
        } else if (table[0] == "O" && table[3] == "O" && table[6] == "O") {
            return true;
        } else if (table[1] == "X" && table[4] == "X" && table[7] == "X") {
            return true;
        } else if (table[1] == "O" && table[4] == "O" && table[7] == "O") {
            return true;
        } else if (table[2] == "X" && table[5] == "X" && table[8] == "X") {
            return true;
        } else if (table[2] == "O" && table[5] == "O" && table[8] == "O") {
            return true;
        } else if (table[3] == "X" && table[4] == "X" && table[5] == "X") {
            return true;
        } else if (table[3] == "O" && table[4] == "O" && table[5] == "O") {
            return true;
        } else if (table[6] == "X" && table[7] == "X" && table[8] == "X") {
            return true;
        } else if (table[6] == "O" && table[7] == "O" && table[8] == "O") {
            return true;
        } else if (table[0] == "X" && table[4] == "X" && table[8] == "X") {
            return true;
        } else if (table[0] == "O" && table[4] == "O" && table[8] == "O") {
            return true;
        } else if (table[2] == "X" && table[4] == "X" && table[6] == "X") {
            return true;
        } else if (table[2] == "O" && table[4] == "O" && table[6] == "O") {
            return true;

        }
        return false;
    }

    public boolean checkWin() {
        if (checkPosition()) {
            savaWin();
            return true;
        }
        return false;
    }

    public void savaWin(){
        if(currentPlayer == player1){
            player1.win();
            player2.lose();
        }else{
            player2.win();
            player1.lose();
        }
    }

    public void switchPlayer() {
        if(currentPlayer == player1){
            currentPlayer = player2;
        }else{
            currentPlayer = player1;
        }
    }
    
    public boolean checkDraw(){
        if(count == 9){
            player1.draw();
            player2.draw();
            return true;
        }
        return false;
    }

    public void reSetXO() {
        table[0] = "1";
        table[1] = "2";
        table[2] = "3";
        table[3] = "4";
        table[4] = "5";
        table[5] = "6";
        table[6] = "7";
        table[7] = "8";
        table[8] = "9";
        count = 0;
    }
    public void resetCurrentPlayer(){
        currentPlayer = player2;
    }
    
    
}
